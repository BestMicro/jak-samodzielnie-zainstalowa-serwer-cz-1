# Instalacja serwera Ubuntu 18.04

  Poniżej przygotowaliśmy Prydatne linki i komendy z których korzystaliśmy w trakcie instalacji naszego serwera.
  
  Możecie również skorzystać z gotowych plików, które tutaj zostały umieszczone:
  * przykładowa [konfiguracja sieciowa][cfg-url]
  * plik z gotowym skryptem do instalacji [webmin.sh][webmin-sh-url]
    * Żeby uruchomić skrypt:
        ```
      sudo chmod +x webmin.sh
      sudo ./webmin.sh
      ```
  * prosty skryp do aktualizacji systemu [up.sh][up-sh-url]
      * Żeby uruchomić skrypt:
        
        ```
        sudo chmod +x up.sh
        sudo ./up.sh
        ```

    

### Zapraszamy do oglądania kolejnych odcinków jak i do naszego sklepu [bestmicro.pl][bestmicro-url] z serwerami i podzespołami serwerowymi


## Przydatne linki
1. [Ubuntu][ubuntu-url] - Strona domowa Ubuntu
2. [BalenaEtcher][etcher-url] - Strona domowa BalenaEtcher
3. [APT][apt-url] - Strona Debiana na której jest opisane działanie, składnia i funkcje APT
4. [htop][htop-url] - Strona domowa HTOP
5. [mc][mc-url] - Strona domowa midnight commander
6. [webmin][webmin-url] - Strona domowa webmin
---
## Przydatne komendy

* Sprawdzenie dostępnej pamięci w komputerze
  ```
  free -h 
  ```  
* Sprawdzanie informacji o procesorze
  ```
  cat /proc/cpuinfo
  ```
  Poklazuje nazwę procesora
  ```
  cat /proc/cpuinfo | grep model
  ```
* Sprawdzenie dysjków i parycji
  ```
  lsblk 
  fdisk -l
  ```
* informacje o kartach sieciowych i ich ustawieniach  
  ```
  ip a
  ```
* zdalne logowanie się do serwera, w miejscu host można podać adres IP
  ```
  ssh user@host 
  ```
* przełączenie się na użytkownika root
  ```
  sudo -s
  ```
---
## Konfiguracja ustawień sieciowych
* utworzenie nowego pliku konfiguracyjnego dla ustawień sieciowych
  ```
  nano /etc/netplan/01-netcfg.yaml
  ```
* Przykładowe ustawiena 
  ```
  # This file describes the network interfaces available on your system
  # For more information, see netplan(5).
  network:
    version: 2
    renderer: networkd
    ethernets:
      enp0s3:
      dhcp4: no
      addresses: [192.168.0.1/24]
      gateway4: 192.168.0.254
      nameservers:
        addresses: [1.1.1.1,1.0.0.1]
  ```
---
## Aktualizowanie systemu
* sprawdzanie dostępnych aktualizacji
  ```
  sudo apt update
  ```
* wyświetla listę pakietów do aktualizacji
  ```
  sudo apt list --upgradable
  ```
* inicjalizacja aktualizacji
  ```
  sudo apt upgrade
  ```
---
## Instalacja Webmin
  ```
  echo "deb http://download.webmin.com/download/repository sarge contrib" | tee -a >> /etc/apt/sorces.list

  wget http://www.webmin.com/jcameron-key.ascsudo apt-key add jcameron-key.asc

  sudo apt update

  sudo apt install webmin

  ```
---
## Licencja

  Wszystkie skrypty i pliki są freesoftware i opensource. 
  Możesz je rozpowszechniać i/lub modyfikować na
  warunkach GNU General Public License Version 3.0. opublikowanych przez
  Free Software Foundation. Kopia GNU GPL 3.0 jest dostarczana wraz z
  oprogramowaniem.

  Po więcej informacji przeczytaj plik [LICENSE][license-local-url] lub odwiedź stronę [GNU GPL-3.0][license-url].


[ubuntu-url]: https://ubuntu.com/
[etcher-url]: https://www.balena.io/etcher/
[apt-url]: https://debian-handbook.info/browse/pl-PL/stable/apt.html
[htop-url]: https://hisham.hm/htop/
[mc-url]: https://midnight-commander.org/
[webmin-url]: http://www.webmin.com/

[license-url]: https://www.gnu.org/licenses/gpl-3.0.en.html
[license-local-url]: https://gitlab.com/BestMicro/jak-samodzielnie-zainstalowa-serwer-cz-1/blob/master/LICENSE
[cfg-url]: https://gitlab.com/BestMicro/jak-samodzielnie-zainstalowa-serwer-cz-1/blob/master/01-netcfg.yml
[webmin-sh-url]: https://gitlab.com/BestMicro/jak-samodzielnie-zainstalowa-serwer-cz-1/blob/master/webmin.sh
[up-sh-url]: https://gitlab.com/BestMicro/jak-samodzielnie-zainstalowa-serwer-cz-1/blob/master/up.sh

[bestmicro-url]: https://bestmicro.pl/